#include <QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Initialize audio format
    audioFormat.setSampleRate(static_cast<int>(sampleRate));
    audioFormat.setChannelCount(1);
    audioFormat.setSampleSize(static_cast<int>(nBytesPerSample) * 8); // Units: bits
    audioFormat.setCodec("audio/pcm");
    audioFormat.setByteOrder(QAudioFormat::Endian(QSysInfo::ByteOrder));
    audioFormat.setSampleType(QAudioFormat::Float);

    // Check audio format
    QAudioDeviceInfo deviceInfo(QAudioDeviceInfo::defaultOutputDevice());
    if (!deviceInfo.isFormatSupported(audioFormat)) {
        qWarning() << "Unsupported audio format";
        return;
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onStateChanged(QAudio::State newState)
{
    switch (newState) {
        case QAudio::IdleState:
            // Finished playing (no more data)
            audio->stop();
            audioBuffer->close();
            delete audio;
            delete audioBuffer;
            delete audioBytes;
            break;

        case QAudio::StoppedState:
            if (audio->error() != QAudio::NoError) {
                qWarning() << "Audio stopped unexpectedly";
            }
            break;

        default:
            break;
    }
}

void MainWindow::on_pushButton_clicked()
{
    qDebug() << "[on_pushButton_clicked]";

    // Initialize audio buffer

    audioBytes = new QByteArray();
    audioBytes->resize(static_cast<int>(nSamples * nBytesPerSample));

    for (unsigned int sample = 0; sample < nSamples; sample++) {
        double data = qSin(2.0 * M_PI * frequency * sample / sampleRate);
        float sampleData = static_cast<float>(data);
        unsigned int offset = sample * nBytesPerSample;
        memcpy(audioBytes->data() + offset, &sampleData, nBytesPerSample);
    }

    audioBuffer = new QBuffer(audioBytes);
    audioBuffer->open(QIODevice::ReadOnly);

    // Initialize audio output

    audio = new QAudioOutput(audioFormat, this);
    connect(
        audio,
        SIGNAL(stateChanged(QAudio::State)),
        this,
        SLOT(onStateChanged(QAudio::State))
    );
    audio->start(audioBuffer);
}
