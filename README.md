# Qt Beeper

This probably leaks memory if you click the button again before the previous
beep finishes.

# Usage
```
sudo apt-get install libqt5multimedia5-plugins
```

# Building
```
sudo apt-get install qtmultimedia5-dev
```
