#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAudioOutput>
#include <QtMath>
#include <QtCore>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void onStateChanged(QAudio::State newState);

private:
    Ui::MainWindow *ui;

    QByteArray* audioBytes;
    QBuffer* audioBuffer;
    QAudioFormat audioFormat;
    QAudioOutput* audio;

    const double frequency = 440;
    const double sampleRate = 44100;
    const double duration = 1.0;
    const unsigned int nSamples = static_cast<uint>(duration * sampleRate);
    const unsigned int nBytesPerSample = sizeof(float);

};
#endif // MAINWINDOW_H
